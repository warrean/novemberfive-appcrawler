Baseball is a bat-and-ball sport, played on a field with bases arranged in a
diamond. After batters hit a thrown ball, they and any teammates on a base can
try to advance to another base, scoring a run when they return to the home
base. The batting team hits against the pitcher of the fielding team, which
tries to get players out (off the field), usually by striking them out,
catching a hit ball, throwing to a base that players have to run to, or tagging
them with the ball between bases. After three outs, the teams trade places, and
after three more, the next inning begins. Professional games last at least into
the ninth inning. Evolving from older bat-and-ball games, an early form of
baseball was being played in England by the mid-eighteenth century. This game
and the related rounders were brought by British and Irish immigrants to North
America, where the modern version of baseball developed. By the late nineteenth
century, baseball was widely recognized as the national sport of the United
States. Baseball has become popular in North America and parts of Central and
South America, the Caribbean, and East Asia, particularly Japan.
