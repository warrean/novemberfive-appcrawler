from functional import partial, map
from functools import reduce
import urllib.request
import json
from bs4 import BeautifulSoup

# Helper functions
# ----------------------------------------

def compose(*functions):
    return reduce(lambda f, g: lambda x: f(g(x)), functions, lambda x: x)

def getHtmlFromUrl(url):
    req = urllib.request.Request(url)
    return urllib.request.urlopen(req).read()

def postDataUrl(url, params):
    req = urllib.request.Request(url, params,
                             headers={'content-type': 'application/json'})
    response = urllib.request.urlopen(req)

def extractAppInfo(html):
    soup = BeautifulSoup(html, 'html.parser')
    return { \
        'appName'       : soup.find("h1", itemprop="name").get_text(), \
        'version'       : soup.find("span", itemprop="softwareVersion").get_text(), \
        'datePublished' : soup.find("span", itemprop="datePublished").get_text().strip(), \
        'iconUrl'       : soup.find("meta", itemprop="image").get('content'), \
        'releaseNotes'  : soup.find_all("div", class_="product-review")[-1].get_text().strip() }

def convertToSlackMessage(appDict):
    bullets = ['• {0}: {1}'.format(k, appDict[k]) for k in ('version', 'datePublished', 'releaseNotes')]
    return '*{0}*:\n{1}'.format(appDict['appName'], "\n".join(bullets))

# Application
# ----------------------------------------

with open('data/input.txt', 'r') as f:
    read_data = f.read()

result = compose(
    partial(map, print),
    partial(map, partial(postDataUrl, 'https://hooks.slack.com/services/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')),
    partial(map, lambda x : json.dumps(x).encode('utf8')),
    partial(map, lambda x : { 'text': x }),
    partial(map, convertToSlackMessage),
    partial(map, extractAppInfo),
    partial(map, getHtmlFromUrl),
    partial(map, lambda x : "https://itunes.apple.com/be/app/{0}".format(x)),
    lambda x : x.splitlines()
    )(read_data)
